from fastapi import FastAPI
from typing import Any
from fastapi.responses import HTMLResponse
from transliterate import translit
import leveldb

db = leveldb.LevelDB('./db')
db.Put(b'hello', b'hello world')
print(db.Get(b'hello').decode('utf-8'))
# async def translit():
#   req.data
app = FastAPI()
cnt = 0
def log(line):
    with open("./logs.log", mode="a+", encoding="utf-8") as f:
        f.write("\n")
        f.write(line)

@app.get("/history")
async def get_history(n: int = 10):
    data = []
    for i in n:
        data.append(db.Get(cnt-i))# todo trycatch
    # data = ["a","b", n]
    return {"data": data }

@app.get("/", response_class=HTMLResponse)
async def index():
    with open("./index.html", mode="r", encoding="utf-8") as f:
        return f.read()

@app.post("/api")
def trans(req: dict):
    if 'data' in req:
        log(req['data'])
        db.Put(cnt,req['data'])

    data=translit(req['data'], 'ru', reversed=True)
    return {"status":"success", "data": data}

